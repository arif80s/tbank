/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controller;

import com.business.BankAcc_Service_I;
import com.model.UserAccountModel;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author abra
 */
@RestController
@RequestMapping("bank")
public class BankAcc_Controller {

    /**
     * @return the bankAcc_Service_I
     */
//    public BankAcc_Service_I getBankAcc_Service_I() {
//        return bankAcc_Service_I;
//    }
//
//    /**
//     * @param bankAcc_Service_I the bankAcc_Service_I to set
//     */
//    public void setBankAcc_Service_I(BankAcc_Service_I bankAcc_Service_I) {
//        this.bankAcc_Service_I = bankAcc_Service_I;
//    }
//
    @Autowired
    private BankAcc_Service_I bankAcc_Service_I;
//    ApplicationContext applicationContext = new ClassPathXmlApplicationContext("spconfig.xml");
//    BankAcc_Service_I ba = (BankAcc_Service_I) applicationContext.getBean("bean_BankAcc_Service_Impl");

 

    public BankAcc_Service_I getBankAcc_Service_I() {
        return bankAcc_Service_I;
    }

//    @RequestMapping(value = "/account/{accNo}")
//    public UserAccountModel getAccount(@PathVariable int accNo) {
//    //public String getAccount(@PathVariable int accNo) {
//        System.out.println("pathvariable is::::"+accNo);
//        System.out.println("\n \n ");
//        System.out.println("inside controller");
//        System.out.println("\n \n ");
    public void setBankAcc_Service_I(BankAcc_Service_I bankAcc_Service_I) {
        this.bankAcc_Service_I = bankAcc_Service_I;
    }

////        return ba.showAccount(accNo);
//         UserAccountModel uam = ba.showAccount(accNo);
//         String name = uam.getAcc_HolderName();
//         System.out.println("name isssss::::"+name);
//        return uam;
//    }
    
    @PostMapping(value = "createAcc")
    public void createAccount(@RequestBody UserAccountModel userAccount) {
        bankAcc_Service_I.createAccount(userAccount);
    }
//
//    @RequestMapping(method = RequestMethod.PUT, value = "/depositAcc")
//    public void depositAccount(int accNo, int depositAmount) {
//        getBankAcc_Service_I().depositAccount(accNo, depositAmount);
//    }
    
    @PutMapping(value = "deposit")
    public void depositAccount(
            @RequestParam("accNo") int accNo,
            @RequestParam("balance") int balance
            ) {
         
        bankAcc_Service_I.depositAccount(accNo, balance);
    }
    
    @PutMapping(value = "transfer")
    public void transferAccount(
            @RequestParam("senderAccNo") int senderAccNo,
            @RequestParam("receiverAccNo") int receiverAccNo,
            @RequestParam("senderBalance") int senderBalance
            //@RequestParam("receiverBalance") int receiverBalance
            ) {
         
        bankAcc_Service_I.transferAccount(senderAccNo, receiverAccNo, senderBalance);
    }
    
//
    @DeleteMapping(value = "delete")
    public void transferAccount(
            @RequestParam("accNo") int accNo) {
         
        bankAcc_Service_I.deleteAccount(accNo);
    }
    
    @GetMapping(value = "show")
    List<Object[]> show(
            @RequestParam("ac_HolderName") String acc_HolderName) {
         
        return bankAcc_Service_I.show(acc_HolderName);
    }
    
    @GetMapping(value = "showListGreaterThanQueryBalance")
    public List<UserAccountModel> showListGreaterThanQueryBalance(
            @RequestParam("balance") int balance) {
         
        return bankAcc_Service_I.findByBalanceGreaterThanQuery(balance);
    }
    
    @GetMapping(value = "findByNomineeLikeQuery")
    public List<UserAccountModel> findByNomineeLikeQuery(
            @RequestParam("nominee") String nominee) {
         
        return bankAcc_Service_I.findByNomineeLikeQuery(nominee);
    }
    
    @GetMapping(value = "findByBalanceLessThanOrAccountNoLessThanQuery")
    public List<UserAccountModel> findByBalanceLessThanOrAccountNoLessThanQuery(
            @RequestParam("balance") int balance, @RequestParam("accNo") int accNo) {
         
        return bankAcc_Service_I.findByBalanceLessThanOrAccountNoLessThanQuery(balance, accNo);
    }
    
    @GetMapping(value = "findByAccBalanceBetweenQuery")
    public List<UserAccountModel> findByAccBalanceBetweenQuery(
            @RequestParam("startBalance") int startBalance, @RequestParam("endBalance") int endBalance) {
         
        return bankAcc_Service_I.findByAccBalanceBetweenQuery(startBalance, endBalance);
    }
//
//    @RequestMapping(method = RequestMethod.PUT, value = "/transfer")
//    public void transferAccount(int senderAccNo, int receiverAccNo, int amount) {
//        bankAcc_Service_I.transferAccount(senderAccNo, receiverAccNo, amount);
//    }


}
