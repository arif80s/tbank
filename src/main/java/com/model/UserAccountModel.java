/*
 * To change this license header, choose License Headembers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author abra
 */
@Entity
@Table(name="UserAccount")
public class UserAccountModel implements Serializable {

    /**
     * @return the acc_Number
     */
    private int version;
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    //private int id;
    //@Column(name="Acc_Number")
    private int acc_Number;
    @Column(name="Name")
    private String acc_holder_name;
    @Column(name="Address")
    private String acc_address;
    @Column(name="Date")
    private String acc_Creation_Date;
    @Column(name="Balance")
    private int acc_balance;
    @Column(name="NomineeeeInfo")
    private String accnomineeInfo;

    public UserAccountModel() {
    }

    public UserAccountModel(int version, int id, int acc_Number, String acc_HolderName, String acc_address, String acc_Creation_Date, int acc_balance, String acc_nomineeInfo) {
        this.version = version;
        //this.id = id;
        this.acc_Number = acc_Number;
        this.acc_holder_name = acc_HolderName;
        this.acc_address = acc_address;
        this.acc_Creation_Date = acc_Creation_Date;
        this.acc_balance = acc_balance;
        this.accnomineeInfo = acc_nomineeInfo;
    }
    
    

    /**
     * @return the version
     */
    public int getVersion() {
        return version;
    }

    /**
     * @param version the version to set
     */
    public void setVersion(int version) {
        this.version = version;
    }

    /**
     * @return the id
     */
//    public int getId() {
//        return id;
//    }
//
//    /**
//     * @param id the id to set
//     */
//    public void setId(int id) {
//        this.id = id;
//    }

    public int getAcc_Number() {
        return acc_Number;
    }

    /**
     * @param acc_Number the acc_Number to set
     */
    public void setAcc_Number(int acc_Number) {
        this.acc_Number = acc_Number;
    }

    /**
     * @return the acc_holder_name
     */
    public String getAcc_holder_name() {
        return acc_holder_name;
    }

    /**
     * @param acc_holder_name the acc_holder_name to set
     */
    public void setAcc_holder_name(String acc_holder_name) {
        this.acc_holder_name = acc_holder_name;
    }

    /**
     * @return the acc_address
     */
    public String getAcc_address() {
        return acc_address;
    }

    /**
     * @param acc_address the acc_address to set
     */
    public void setAcc_address(String acc_address) {
        this.acc_address = acc_address;
    }

    /**
     * @return the acc_Creation_Date
     */
    public String getAcc_Creation_Date() {
        return acc_Creation_Date;
    }

    /**
     * @param acc_Creation_Date the acc_Creation_Date to set
     */
    public void setAcc_Creation_Date(String acc_Creation_Date) {
        this.acc_Creation_Date = acc_Creation_Date;
    }

    /**
     * @return the acc_balance
     */
    public int getAcc_balance() {
        return acc_balance;
    }

    /**
     * @param acc_balance the acc_balance to set
     */
    public void setAcc_balance(int acc_balance) {
        this.acc_balance = acc_balance;
    }

    /**
     * @return the accnomineeInfo
     */
    public String getAccnomineeInfo() {
        return accnomineeInfo;
    }

    /**
     * @param accnomineeInfo the accnomineeInfo to set
     */
    public void setAccnomineeInfo(String accnomineeInfo) {
        this.accnomineeInfo = accnomineeInfo;
    }

}
