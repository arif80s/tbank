/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.business;

import com.model.UserAccountModel;
import com.persistence.BankAcc_CrudRepo_I;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 *
 * @author abra
 */
@Component
@Qualifier("RealService")
public class BankAccount_Service_Impl implements BankAcc_Service_I {

    /**
     * @return the bankAcc_Persistence_I
     */
 
    @Autowired
    private BankAcc_CrudRepo_I bankAcc_Crud_I;

    public BankAcc_CrudRepo_I getBankAcc_Crud_I() {
        return bankAcc_Crud_I;
    }

    public void setBankAcc_Crud_I(BankAcc_CrudRepo_I bankAcc_Crud_I) {
        this.bankAcc_Crud_I = bankAcc_Crud_I;
    }

 

    /**
     * @param bankAcc_Persistence_I the bankAcc_Persistence_I to set
     */
 

    @Override
    public void createAccount(UserAccountModel userAccount) {
        //getBankAcc_Persistence_I().create(userAccount);
        bankAcc_Crud_I.save(userAccount);
    }

    @Override
    public void depositAccount(int accNo, int balance) {
        int previousBalance;
        int accNumber;
        //id = bankAcc_Persistence_I.selectId_ByAccNo(accNo);
        //previousBalance = bankAcc_Persistence_I.selectBalanceByAccNo(accNo);
        //System.out.println("current balance :::" + previousBalance);

        // bankAcc_Persistence_I.update(accNo, balance + previousBalance);
        UserAccountModel uam = bankAcc_Crud_I.findOne(accNo);
        previousBalance = uam.getAcc_balance();
        uam.setAcc_balance(balance + previousBalance);
        bankAcc_Crud_I.save(uam);

    }

//    @Override
//    public int withdrawAccount(int accNo, int withdrawAmount) {
//        int previousBalance;
//     //   int id;
//        int afterWithdraw;
//        int flug;
//       // id = bankAcc_Persistence_I.selectId_ByAccNo(accNo);
//        //System.out.println("idddddd::::" + id);
//        //previousBalance = bankAcc_Persistence_I.selectBalance(accNo);
//        System.out.println("previous balance is::::" + previousBalance);
//        afterWithdraw = previousBalance - withdrawAmount;
//        System.out.println("after withdraw isss:::" + afterWithdraw);
//
//        if (withdrawAmount > 10000 || afterWithdraw < 500) {
//            flug = 0;
//        } else {
//            flug = bankAcc_Persistence_I.update(accNo, afterWithdraw);
//
//        }
//        return flug;
//
//    }
//    @Override
//    public int transferAccount(int senderAccNo, int receiverAccNo, int amount) {
//        int flug;
//        //int senderId;
////        int receiverId;
//        int senderPreviousBalance;
//        int receiverPreviousBalance;
//        int amountAfterSending;
//        int amountAfterReceiving;
//        boolean objectExist;
////        senderId = bankAcc_Persistence_I.selectId_ByAccNo(senderAccNo);
////        receiverId = bankAcc_Persistence_I.selectId_ByAccNo(receiverAccNo);
//        senderPreviousBalance = bankAcc_Persistence_I.selectBalance(senderAccNo);
//        receiverPreviousBalance = bankAcc_Persistence_I.selectBalance(receiverAccNo);
//        amountAfterSending = senderPreviousBalance - amount;
//        amountAfterReceiving = receiverPreviousBalance + amount;
//        objectExist = bankAcc_Persistence_I.objectExist(receiverAccNo);
//        if (objectExist) {
//            if (amount <= 10000 && amountAfterSending >= 500) {
//                flug = bankAcc_Persistence_I.transferAccount(senderAccNo, amountAfterSending, receiverAccNo, amountAfterReceiving);
//            } else {
//                flug = 0;
//            }
//        } else {
//            flug = 0;
//        }
//        return flug;
//    }
//    @Override
//    public UserAccountModel showAccount(int accNo) {
//        //int userId;
//        System.out.println("\n \n ");
//        System.out.println("inside showAccount service impl");
//        System.out.println("\n \n ");
//        //userId = bankAcc_Persistence_I.selectId_ByAccNo(accNo);
//        System.out.println("\n \n after userId\n \n");
//        return bankAcc_Persistence_I.showAccount(accNo);
//    }

    @Override
    public int withdrawAccount(int accNo, int withdrawAmount) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void transferAccount(int senderAccNo, int receiverAccNo, int amount) {
        int senderPreviousBalance;
        int receiverPreviousBalance;
        int amountAfterSending;
        int amountAfterReceiving;
        boolean objectExist;

        if (bankAcc_Crud_I.exists(receiverAccNo) && amount <= 10000) {
            UserAccountModel uamSender = bankAcc_Crud_I.findOne(senderAccNo);
            UserAccountModel uamReceiver = bankAcc_Crud_I.findOne(receiverAccNo);
            senderPreviousBalance = uamSender.getAcc_balance();
            amountAfterSending = senderPreviousBalance - amount;
            receiverPreviousBalance = uamReceiver.getAcc_balance();
            amountAfterReceiving = receiverPreviousBalance + amount;
            if(amountAfterSending >= 500){
                uamSender.setAcc_balance(amountAfterSending);
                bankAcc_Crud_I.save(uamSender);
                uamReceiver.setAcc_balance(amountAfterReceiving);
                bankAcc_Crud_I.save(uamReceiver);
            }
        }
        //testing purpose
        //int a = 3;

//        senderPreviousBalance = bankAcc_Persistence_I.selectBalanceByAccNo(senderAccNo);
//        receiverPreviousBalance = bankAcc_Persistence_I.selectBalanceByAccNo(receiverAccNo);
//        amountAfterSending = senderPreviousBalance - amount;
//        amountAfterReceiving = receiverPreviousBalance + amount;
//        objectExist = bankAcc_Persistence_I.objectExist(receiverAccNo);
//        
//        if (objectExist) {
//            if (amount <= 10000 && amountAfterSending >= 500) {
//                bankAcc_Persistence_I.transferAccount(senderAccNo, amountAfterSending, receiverAccNo, amountAfterReceiving);
//            }
//        }
    }

    @Override
    public void deleteAccount(int accNo) {

//        bankAcc_Persistence_I.delete(accNo);
        bankAcc_Crud_I.delete(accNo);

    }

    @Override
    public UserAccountModel showAccount(int accNo) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object[]> show(String acc_HolderName) {
       return bankAcc_Crud_I.findByAcc_holder_name(acc_HolderName);
    }

    @Override
    public List<UserAccountModel> findByBalanceGreaterThanQuery(int balance) {
        return bankAcc_Crud_I.findByBalanceGreaterThanQuery(balance);
    }

    @Override
    public List<UserAccountModel> findByNomineeLikeQuery(String nominee) {
        return bankAcc_Crud_I.findByNomineeLikeQuery(nominee);
    }

    @Override
    public List<UserAccountModel> findByBalanceLessThanOrAccountNoLessThanQuery(int balance, int accNo) {
        return bankAcc_Crud_I.findByBalanceLessThanOrAccountNoLessThanQuery(balance, accNo);
    }

    @Override
    public List<UserAccountModel> findByAccBalanceBetweenQuery(int start, int end) {
        return bankAcc_Crud_I.findByAccBalanceBetweenQuery(start, end);
    }
}
