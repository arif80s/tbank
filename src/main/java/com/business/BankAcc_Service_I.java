/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.business;

import com.model.UserAccountModel;
import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Service;

/**
 *
 * @author abra
 */
@Service
public interface BankAcc_Service_I {

    public void createAccount(UserAccountModel userAccount);

    public void depositAccount(int accNo, int balance);

    public int withdrawAccount(int accNo, int withdrawAmount);

    public void transferAccount(int senderAccNo, int receiverAccNo, int amount);

    public UserAccountModel showAccount(int accNo);

    public void deleteAccount(int accNo);

    public List<Object[]> show(String acc_HolderName);

    public List<UserAccountModel> findByBalanceGreaterThanQuery(int balance);
    
    public List<UserAccountModel> findByNomineeLikeQuery(String nominee);
    
    public List<UserAccountModel> findByBalanceLessThanOrAccountNoLessThanQuery(int balance, int accNo);
    
    public List<UserAccountModel> findByAccBalanceBetweenQuery(int start, int end);

}
