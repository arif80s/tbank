/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.persistence;

import com.model.UserAccountModel;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author abra
 */
public interface BankAcc_CrudRepo_I extends CrudRepository<UserAccountModel, Integer> {
//    @Override
//    List<UserAccountModel> findAll();

    /**
     *
     * @param uam
     * @return
     */
    @Override
    public UserAccountModel save(UserAccountModel uam);

    @Override
    public UserAccountModel findOne(Integer id);

    @Override
    public void delete(Integer id);

    @Query("select c.acc_Number, c.acc_balance from UserAccountModel c where c.acc_holder_name = :countryName")
    List<Object[]> findByAcc_holder_name(@Param("countryName") String countryName);

    @Query("select c from UserAccountModel c where c.acc_balance > ?1")
    public List<UserAccountModel> findByBalanceGreaterThanQuery(int balance);

    @Query("select c from UserAccountModel c where c.accnomineeInfo like ?1")
    public List<UserAccountModel> findByNomineeLikeQuery(String nominee);

//    public List<UserAccountModel> readByAccnomineeInfoStartingWith(String searchTerm);
//    
//    public List<UserAccountModel> readByAccnomineeInfoContaining(String searchTerm);
    @Query("select c from UserAccountModel c where c.acc_balance < ?1 or c.acc_Number < ?2")
    public List<UserAccountModel> findByBalanceLessThanOrAccountNoLessThanQuery(int balance, int accNo);

    @Query("select c from UserAccountModel c where c.acc_balance > ?1 and c.acc_balance < ?2")
    public List<UserAccountModel> findByAccBalanceBetweenQuery(int start, int end);

}
