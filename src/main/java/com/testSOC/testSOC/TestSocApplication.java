package com.testSOC.testSOC;

import com.controller.BankAcc_Controller;
import com.controller.TopicController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories("com.persistence")
@ComponentScan(basePackageClasses = BankAcc_Controller.class)
@ImportResource("classpath:spconfig.xml")
public class TestSocApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestSocApplication.class, args);
	}
}
